<?php
/*
Plugin Name: WP Dev Helper
Plugin URI: http://tivlabs.com
Description: Several helper functions for development in WordPress
Version: 0.1
Author: Hemant Arora
Author URI: http://tivlabs.com/
*/

/* Add sanitized widget title as a class to 'before_widget' */
add_filter('dynamic_sidebar_params', 'wdh_dynamic_sidebar_params');
function wdh_dynamic_sidebar_params($params) {
	$wdgtvar = 'widget_'._get_widget_id_base($params[0]['widget_id']);
	$instance = get_option($wdgtvar);
	$idvar = _get_widget_id_base($params[0]['widget_id']);
	$idbs = str_replace($idvar.'-', '', $params[0]['widget_id']);
	$params[0]['before_widget'] = str_replace('class="', 'class="'.sanitize_title($instance[$idbs]['title']).' ', $params[0]['before_widget']);
	return $params;
}

/* Get post excerpt */
function wdh_get_post_excerpt($post_id, $excerpt_length = 35){
	$the_post = get_post($post_id); //Gets post ID
	$the_excerpt = apply_filters('the_excerpt', get_post_field('post_excerpt', $post_id));
	if(empty($the_excerpt)) {
		$the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
		$the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
	}
	$words = explode(' ', $the_excerpt, $excerpt_length + 1);
	if(count($words) > $excerpt_length) {
		array_pop($words);
		array_push($words, '…');
		$the_excerpt = implode(' ', $words);
	}
	return $the_excerpt;
}

// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');

/* Shortcode that fires 'get_template_part' */
add_shortcode('wdh_get_template_part', 'wdh_shortcode_get_template_part');
function wdh_shortcode_get_template_part($atts = array(), $content = '') {
	$atts = shortcode_atts(array(
		'slug'         => '',
		'name'         => '',
		'replace'      => '',
		'replace_with' => ''
	), $atts, 'siml');
	extract($atts);
	if(empty($slug)) return '';
	ob_start();
	get_template_part($slug, $name);
	$template_part = ob_get_clean();
	if(trim($replace) != '')
		$template_part = str_replace(explode(',', $replace), explode(',', $replace_with), $template_part);
	return $template_part;
}

// Dynamic bucket widget for sidebars
add_action('widgets_init', 'wdh_widgets_init');
function wdh_widgets_init() {
	register_widget('wdh_dynamic_bucket_widget');
}
add_action('admin_enqueue_scripts', 'wdh_admin_enqueue_scripts');
function wdh_admin_enqueue_scripts() {
	wp_enqueue_media();
	wp_enqueue_script('wdh-admin-script', plugins_url('wp-dev-helper/js/admin-script.js'), false, '1.0', true);
}
class wdh_dynamic_bucket_widget extends WP_Widget {
	function __construct() {
		parent::__construct('wdh_dynamic_bucket_widget', __('WDH Dynamic Bucket', 'wdh'), array( 'description' => __( 'Create a bucket with the layout of your choice.', 'wdh')));
	}
	public function widget($args, $instance) {
		echo $args['before_widget'];
		$layout = $instance['layout'];
		$layout = str_replace('[TITLE]', apply_filters('widget_title', $instance['title']), $layout);
		$layout = str_replace('[CONTENT]', $instance['content'], $layout);
		$layout = str_replace('[BG_IMAGE]', $instance['bg_image'], $layout);
		$layout = str_replace('[LINK]', $instance['link'], $layout);
		echo $layout;
		echo $args['after_widget'];
	}
	public function form($instance) {
		$title = isset($instance['title']) ? $instance['title'] : __('New title', 'wdh'); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'wdh'); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		</p><?php
		$content = isset($instance['content']) ? $instance['content'] : __('', 'wdh'); ?>
		<p>
			<label for="<?php echo $this->get_field_id('content'); ?>"><?php _e('Content', 'wdh'); ?></label> 
			<textarea class="widefat" id="<?php echo $this->get_field_id('content'); ?>" name="<?php echo $this->get_field_name('content'); ?>"><?php echo esc_attr($content); ?></textarea>
		</p><?php
		$bg_image = isset($instance['bg_image']) ? $instance['bg_image'] : __('', 'wdh'); ?>
		<div class="wdh-dbw-bg_image-field">
			<p style="margin-bottom: 0"><label for="<?php echo $this->get_field_id('bg_image'); ?>"><?php _e('Background Image', 'wdh'); ?></label></p>
			<table style="margin-bottom: 1em; width: 100%">
				<tr>
					<td style="padding: 0; vertical-align: top; width: 80px">
						<span class="wdh-dbw-bg_image-preview" style="background: #DDD <?php echo empty($bg_image) ? 'none' : 'url('.$bg_image.')'; ?> center center no-repeat; background-size: cover; border: 1px solid rgba(0,0,0,0.15); display: block; width: 100%; height: auto; padding-top: 100%;"></span>
					</td>
					<td style="padding: 0 0 0 10px; vertical-align: top">
						<input class="widefat wdh-dbw-bg_image-url" id="<?php echo $this->get_field_id('bg_image'); ?>" name="<?php echo $this->get_field_name('bg_image'); ?>" type="text" value="<?php echo esc_attr($bg_image); ?>" />
						<input type="button" value="Upload Image" class="button wdh-dbw-bg_image-button" id="<?php echo $this->get_field_id('bg_image'); ?>-button" style="display: table; margin-top: 4px" />
					</td>
				</tr>
			</table>
		</div>
		<?php
		$link = isset($instance['link']) ? $instance['link'] : __('', 'wdh'); ?>
		<p>
			<label for="<?php echo $this->get_field_id('link'); ?>"><?php _e('Link URL', 'wdh'); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo esc_attr($link); ?>" />
		</p><?php
		$layout = isset($instance['layout']) ? $instance['layout'] : '<div class="wdh-dynamic-bucket '.sanitize_title($title).'" style="background-image: url([BG_IMAGE])">'."\r\n\t".'<a href="[LINK]">[TITLE]</a>'."\r\n\t".'[CONTENT]'."\r\n".'</div>'; ?>
		<p>
			<span class="wdh-dbw-layout-default"><a href="javascript: void(0)">Change layout (HTML)...</a></span>
			<span class="wdh-dbw-layout-custom" style="display: none">
				<label for="<?php echo $this->get_field_id('layout'); ?>"><?php _e('Layout (HTML)', 'wdh'); ?></label> 
				<textarea class="widefat" id="<?php echo $this->get_field_id('layout'); ?>" name="<?php echo $this->get_field_name('layout'); ?>" style="height: 180px"><?php echo esc_attr($layout); ?></textarea>
				<span class="description" style="display: table">The values of fields above can be used in this field using the templates <code>[TITLE]</code>, <code>[CONTENT]</code>, <code>[BG_IMAGE]</code> and <code>[LINK]</code></span>
			</span>
		</p><?php
	}
	public function update($new_instance, $old_instance) {
		$instance = array();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		$instance['content'] = (!empty($new_instance['content'])) ? $new_instance['content'] : '';
		$instance['bg_image'] = (!empty($new_instance['bg_image'])) ? strip_tags($new_instance['bg_image']) : '';
		$instance['link'] = (!empty($new_instance['link'])) ? strip_tags($new_instance['link']) : '';
		$instance['layout'] = (!empty($new_instance['layout'])) ? $new_instance['layout'] : '';
		return $instance;
	}
}

?>