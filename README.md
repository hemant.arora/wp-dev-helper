# WP Dev Helper

**Author:** Hemant Arora
**Company:** TivLabs

This WordPress plugin is meant to provide various general purpose tools needed
when developing customized WordPress themes and plugins.

Below are the key features included:
 - Add sanitized widget title as a class to sidebar widgets
 - Enable shortcodes in widgets
 - Post excerpt without breaking words
 - Shortcode that includes template parts (similar to the function get_template_part())
