jQuery(document).ready(function($) {
	var _custom_media = true,
		_orig_send_attachment = wp.media.editor.send.attachment;
	$('body')
		.on('click', '.button.wdh-dbw-bg_image-button', function(e) {
			e.preventDefault();
			_custom_media = true;
			var send_attachment_bkp = wp.media.editor.send.attachment,
				$button = $(this),
				field_id = $button.attr('id').replace('-button', '');
			wp.media.editor.send.attachment = function(props, attachment){
				if(_custom_media) {
					$('#'+field_id).val(attachment.url);
					$('#'+field_id).closest('.wdh-dbw-bg_image-field').find('.wdh-dbw-bg_image-preview').css('background-image', 'url('+attachment.url+')');
				} else {
					return _orig_send_attachment.apply(this, [props, attachment]);
				}
			}
			wp.media.editor.open();
			return false;
		})
		.on('click', '.wdh-dbw-layout-default', function(e) {
			e.preventDefault();
			jQuery(this).hide().siblings('.wdh-dbw-layout-custom').show();
		});
	$('.add_media').on('click', function(){
		_custom_media = false;
	});
});

/*jQuery(document).ready(function() {
	function media_upload(button_class) {
		var _custom_media = true,
		_orig_send_attachment = wp.media.editor.send.attachment;
		jQuery('body').on('click', '.custom_media_upload', function(e) {
			var button_id = '#'+jQuery(this).attr('id'),
				button_id_s = jQuery(this).attr('id'),
				self = jQuery(button_id),
				send_attachment_bkp = wp.media.editor.send.attachment,
				button = jQuery(button_id),
				id = button.attr('id').replace('_button', '');
			_custom_media = true;
			
			wp.media.editor.send.attachment = function(props, attachment) {
				if(_custom_media) {
					jQuery('.' + button_id_s + '_media_id').val(attachment.id);
					jQuery('.' + button_id_s + '_media_url').val(attachment.url);
					jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css('display', 'block');
				} else {
					return _orig_send_attachment.apply(button_id, [props, attachment]);
				}
			};
			wp.media.editor.open(button);
			return false;
		});
	}
	media_upload('.custom_media_upload');
});*/